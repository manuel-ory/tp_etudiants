<?php
  require_once 'src/Poneys.php';
  require_once 'src/NumberPoneyNegative.php';
  require_once 'src/TooMuchPoney.php';

use PHPUnit\Framework\TestCase;

  class PoneysTest extends TestCase {

    private $Poneys;

    protected function setUp(){
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(NUMBER_PONEYS);
    }
    /**
    * @dataProvider removeProvider
    */
    public function test_removePoneyFromField($numberToRemove, $expected) {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField($numberToRemove);
      
      // Assert
      $this->assertEquals($expected, $this->Poneys->getCount());
    }

  /**
  *removeProvider contient un set de Tests pour le test de la 
  * méthode removePoneyFromField
  */
    public function removeProvider(){
      return [
          [3,NUMBER_PONEYS-3],
          [5,NUMBER_PONEYS-5],
          [1,NUMBER_PONEYS-1]
      ];
    }

    /**
     * Test pour tester si nombre négatif de poneys dans le champ
     * @expectedException NumberPoneyNegative
     */
    public function test_exceptionRemovePoneyFromField() {
      // Setup
      //$Poneys = new Poneys();

      // Action  
      $this->Poneys->removePoneyFromField(10);   
    }



    public function test_addPoneyInField() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys->addPoneyInField(2);

      // Assert
      $this->assertEquals(10, $this->Poneys->getCount());
    }

    /**
    * @expectedException TooMuchPoney
    */
    public function test_exceptionAddPoneyInField(){
        // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys->addPoneyInField(8);
    }



    public function test_getNamesPoneys(){

      $response = array('a','b','c');
      $this->markTestSkipped();
      $PoneysMock = $this->getMock('Poneys');
      $PoneysMock->expects($this
      ->once())
      ->method('getNames')
      ->willReturn($response);
      $this->assertEquals(array('a','b','c'),$PoneysMock->getNames());

    }

    public function test_fieldIsFree(){
      //$Poneys = new Poneys();

      //Au début, 8 poneys, donc il y a de la place
      $this->assertTrue($this->Poneys->fieldIsFree());

      //On ajoute 7 poneys
      $this->Poneys->addPoneyInField(7);

      //on arrive à 15, il ne doit plus y avoir de place
      $this->assertFalse($this->Poneys->fieldIsFree());

    }

    public function tearDown(){
      unset($this->Poneys);
    }

  }
 ?>
