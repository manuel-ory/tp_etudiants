<?php 
  class Poneys {
      private $count = 8; 
     


      public function getCount() {
        return $this->count;
      }

      public function setCount($numberPoneys){
        
        if ($numberPoneys>MAX_PONEYS){
          throw new TooMuchPoney('Le nombre de poneys dépasse le nombre maximum');
        }

        $this->count=$numberPoneys;
      }

      public function removePoneyFromField($number) {
        if(($this->count - $number)<0){
          throw new NumberPoneyNegative('Nombre négatif de poneys dans le champ');
        }
        else {
          $this->count -= $number;
        }
        
      }

      public function getNames() {

      }

      public function addPoneyInField($number) {
        if(($this->count + $number)>MAX_PONEYS){
          throw new TooMuchPoney('Pas assez d\'espace');
        }
        else {
          $this->count += $number;
        }
        
      }

      public function fieldIsFree() {
        return !($this->count == MAX_PONEYS);
      }

  }
?>
